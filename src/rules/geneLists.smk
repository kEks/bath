rule initRadomeList:
    input:
        cfgGO
    output:
        o=temp(expand(outputDic+"/ZS/GO_Genes/filler/{ID}", ID=GOdf[GOdf['type']=="random"]['path']))
    conda: 
        config["condaEnv"]
    script:
        config["srcFolder"]+"/scripts/A_initRandomList.R"

rule creatGeneRefFile:
    params:
        refrenceSet,
        refHost
    output:
        outputDic+"/ZS/geneRefFiles/annot_df_"+"refrenceSet"+".Rds"
    conda: 
        config["condaEnv"]
    script:
        config["srcFolder"]+"/scripts/A_creatRefSet.R"


rule includeRandomList:  
    input:
        ID=outputDic+"/ZS/GO_Genes/filler/{ID}",
        ref=outputDic+"/ZS/geneRefFiles/annot_df_"+"refrenceSet"+".Rds"
    output:
        o=outputDic+"/ZS/GO_Genes/goCollection_RAN{ID}.csv"
    conda: 
        config["condaEnv"]
    script:
        config["srcFolder"]+"/scripts/A_creatRanGeneList.R"

rule includeExternalGeneList:  
    input:
        lambda wc: GOdf[(GOdf.ID == wc.goID) & (GOdf.type == "extList")].path.iloc[0]
    output:
        outputDic+"/ZS/GO_Genes/goCollection_{goID}.csv"
    shell:
        "cp {input} {output}"


rule mergeGeneSets:
    input:
        i=[expand(outputDic+"/ZS/GO_Genes/goCollection_{goID}.csv" ,goID=list(set(GOdf['ID'])))]
    output:
        o=outputDic+"/ZS/mergedGeneDF.csv"
    conda: 
        config["condaEnv"]
    script:
       config["srcFolder"]+"/scripts/B_mergeGeneLists.R"

