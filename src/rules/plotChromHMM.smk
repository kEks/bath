rule prettyEmission:
    input:
        i1=cfgTissue,
        i2=cfgStates,
        i3=emissionPath
    params:
        p4=segPath
    output:
        o1=outputDic+ "/results/emissionTable.png",
        o2=outputDic+ "/ZS/emissionTable.rds"
    conda: 
        config["condaEnv"]
    script:
        config["srcFolder"]+"/scripts/Z_prettyEmission.R"

