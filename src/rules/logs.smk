rule logEnv:
    output:
        outputDic+"/log/CondaEnv.yml"
    priority: -10
    conda: 
        config["condaEnv"]
    shell:
        "conda env export > {output}"

rule rulegraph:
    output:
        r=outputDic+"/log/rulegraph.pdf",
        d=outputDic+"/log/dag.pdf"
    priority: -10
    params: 
        cfg=conf_path,
        sf=snakefile_dicPath
    conda: 
        config["rulgraphEnv"]
    shell:
        """
        cd {params.sf}/
        snakemake --configfile {params.cfg} --forceall --rulegraph | dot -Tpdf > {output.r}
        snakemake --configfile {params.cfg} --forceall --dag | dot -Tpdf > {output.d}
        """