rule CLESCalc:
    input:
        modleInput=outputDic+"/ZS/modle/modleInput_set.rds",
        modleOutput=outputDic+"/ZS/modle/bayesModle.Rds",
        lookup=outputDic+"/ZS/lookupTable.rds",
        stateCfgPath=cfgStates,
        tissueCFGPath=cfgTissue
    output:
        rds=outputDic+"/ZS/CLESTables/CLESTable.rds"
    conda: 
        config["condaEnv"]
    threads: 8
    script:
        config["srcFolder"]+"/scripts/F_CLESCalc.R"


rule CLESPlot:
    input:
        rds=outputDic+"/ZS/CLESTables/CLESTable.rds",
        stateCfgPath=cfgStates,
        tissueCFGPath=cfgTissue
    params:
        p="{goID}"
    output:
        png=outputDic+"/results/CLES/CLES_{goID}.png"
    conda: 
        config["condaEnv"]
    threads: 1
    script:
        config["srcFolder"]+"/scripts/G_CLESPlot.R"


rule CLES_of_P_Plot:
    input:
        rds=outputDic+"/ZS/CLESTables/CLESTable.rds",
        stateCfgPath=cfgStates,
        tissueCFGPath=cfgTissue
    params:
        p="{goID}"
    output:
        png=outputDic+"/results/CLES_of_p/CLES_of_p_{goID}.png"
    conda: 
        config["condaEnv"]
    threads: 1
    script:
        config["srcFolder"]+"/scripts/G_CLES_of_p_Plot.R"
