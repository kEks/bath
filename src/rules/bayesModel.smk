rule preppingDataForStan:
    input:
        cfgGOPath = cfgGO,
        hitCountTablePaths = [expand(outputDic+"/ZS/transitionHitsCounted/transitionHitCount_{combID}.rds" ,combID=list(set(combTable['transID'])))]
    output:
        outPathDat =  outputDic+"/ZS/modle/modleInput_dat.rds",
        outputPathInput_set =  outputDic+"/ZS/modle/modleInput_set.rds",
        outputPathInput_all =  outputDic+"/ZS/modle/modleInput_all.rds",
        outputPathLookup =  outputDic+"/ZS/lookupTable.rds"
    conda: 
        config["condaEnv"]
    threads: 8
    script:
      config["srcFolder"]+"/scripts/D_preppingHitTableForStan.R" 

rule runStanModle:
    input:
        i=outputDic+"/ZS/modle/modleInput_dat.rds"
    params:
        p=config["srcFolder"]+"/scripts/stanCode/multiBetaBinom_v13b.stan",
    output:
        modleOutput=outputDic+"/ZS/modle/bayesModle.Rds"
    conda: 
        config["condaEnv"]
    threads: 8
    script:
        config["srcFolder"]+"/scripts/E_runningStan.R" 

rule modleValiations:
    input:
        modle=outputDic+"/ZS/modle/bayesModle.Rds",
        ModleInput_all=outputDic+"/ZS/modle/modleInput_all.rds",
        ModleInput_set=outputDic+"/ZS/modle/modleInput_set.rds",
        dat=outputDic+"/ZS/modle/modleInput_dat.rds"
    output:
        o1=outputDic+ "/results/modleQuality/PPC_histos/individuals/README",
        o2=outputDic+ "/results/modleQuality/rhat.png"
    conda: 
        config["condaEnv"]
    script:
        config["srcFolder"]+"/scripts/F_ModleCheck.R"

