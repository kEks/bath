rule mapTransiton2Genes:
    input:
        iref=outputDic+"/ZS/geneRefFiles/annot_df_"+"refrenceSet"+".Rds",
        fA=lambda wc: combTable[combTable.transID == wc.combID].pathA.iloc[0]+"/"+combTable[combTable.transID == wc.combID].fileNameA.iloc[0],
        fB=lambda wc: combTable[combTable.transID == wc.combID].pathB.iloc[0]+"/"+combTable[combTable.transID == wc.combID].fileNameB.iloc[0]
    params:
        p="{combID}"
    output:
        o1a=temp(outputDic+"/ZS/transitionMapped2Genes/transitonVis_{combID}.bed"),
        o1b=temp(outputDic+"/ZS/transitionMapped2Genes/transitionGeneVis_{combID}.bed"),
        o2=outputDic+"/ZS/transitionMapped2Genes/transitionGeneDF_{combID}.rds"
    conda: 
        config["condaEnv"]
    script:
        config["srcFolder"]+"/scripts/B_mappingChromHMM.R"
        

rule creatHitTable:
    input:
        i1=outputDic+"/ZS/transitionMapped2Genes/transitionGeneDF_{combID}.rds",
        i2=outputDic+"/ZS/mergedGeneDF.csv"
    params:
        cID="{combID}",
        intraCellIndex = lambda wc: combTable[combTable.transID == wc.combID].intraCell.iloc[0],
        tissueA = lambda wc: combTable[combTable.transID == wc.combID].tissueA.iloc[0],
        tissueB = lambda wc: combTable[combTable.transID == wc.combID].tissueB.iloc[0]
    output:
        o=outputDic+"/ZS/transitionHitsCounted/transitionHitCount_{combID}.rds"
    conda: 
        config["condaEnv"]
    script:
        config["srcFolder"]+"/scripts/C_creatHitTable_mergedGeneLists.R"
