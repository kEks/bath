
library(readr)
library(extraDistr)
library(svMisc)
library(rstan)

path2Modle <- snakemake@input[["modle"]]
path2dat <- snakemake@input[["dat"]]
path2output <- snakemake@output[["o"]]

if (FALSE) { #debug section
    path2Modle <- "/media/Enbio/Christoph/PhD-Work_DataUScrips/BATH/Reproducing_2023-Publication/ROADMAP/5_BATH/ZS/modle/bayesModle.Rds"
    path2dat <- "/media/Enbio/Christoph/PhD-Work_DataUScrips/BATH/Reproducing_2023-Publication/ROADMAP/5_BATH/ZS/modle/modleInput_dat.rds"
    path2output <- "/media/Enbio/Christoph/PhD-Work_DataUScrips/BATH/Reproducing_2023-Publication/ROADMAP/5_BATH/ZS/modle/bayesModle_postAdded.Rds"

}
modle <- read_rds(path2Modle)
post <- rstan::extract(modle)
rm(modle)

dat <- read_rds(path2dat)

set.seed(1)
randomDraws <- sample(seq(8000), 2000)

message("Staring with all genes")
# creating rep
timeStart<-Sys.time()
listOfna_rep <- list()
for (n in seq(length(dat$a_t))) {
    if (n %% 1000 == 0) { 
        timeEnd<-Sys.time()
        difference <- difftime(timeEnd, timeStart, units='mins')
        message(paste(round(difference,digits = 1),"min  |  ",100*round(n/length(dat$a_t), digits = 2),"%  |  ",n, "of", length(dat$a_t)))    }
    Pa_alpha_tmp <- post$Pa_alpha[randomDraws, dat$a_t[n], dat$a_b[n]]
    Pa_beta_tmp <- post$Pa_beta[randomDraws, dat$a_t[n], dat$a_b[n]]
    N <- dat$a_total[n]
    na_rep_tmp <- mapply(rbbinom, n=1, size=N, alpha=Pa_alpha_tmp , beta=Pa_beta_tmp )
    if (n == 1 | (n-1) %% 10000 == 0) {    na_rep <- na_rep_tmp    }
    if (n != 1 & (n-1) %% 10000 != 0) {  na_rep <- cbind(na_rep, na_rep_tmp)}
    
    if (n %% 10000 == 0 | n == length(dat$a_t)) { 
        listOfna_rep[[paste0("upTo",n,"Rows")]]<- na_rep
        rm(na_rep)
        }
}
difference <- difftime(timeEnd, timeStart, units='mins')
message(paste(round(difference,digits = 1),"min  |  ",100*round(n/length(dat$a_t), digits = 2),"  |  ",n, "of", length(dat$a_t)))    
message("Finished with all genes\n\n Staring with set genes")


timeStart<-Sys.time()
listOfns_rep <- list()
for (n in seq(length(dat$s_t))) {
    if (n %% 1000 == 0) { 
        timeEnd<-Sys.time()
        difference <- difftime(timeEnd, timeStart, units='mins')
        message(paste(round(difference,digits = 1),"min  |  ",100*round(n/length(dat$s_t), digits = 2),"%  |  ",n, "of", length(dat$s_t)))    }
    Ps_alpha_tmp <- post$Ps_alpha[randomDraws, dat$s_t[n], dat$s_b[n], dat$s_g[n]]
    Ps_beta_tmp <- post$Ps_beta[randomDraws, dat$s_t[n], dat$s_b[n], dat$s_g[n]]
    N <- dat$s_total[n]
    ns_rep_tmp <- mapply(rbbinom, n=1, size=N, alpha=Ps_alpha_tmp , beta=Ps_beta_tmp )
    if (n == 1 | (n-1) %% 10000 == 0) {    ns_rep <- ns_rep_tmp    }
    if (n != 1 & (n-1) %% 10000 != 0) {  ns_rep <- cbind(ns_rep, ns_rep_tmp)}
    
    if (n %% 10000 == 0 | n == length(dat$s_t)) { 
        listOfns_rep[[paste0("upTo",n,"Rows")]]<- ns_rep
        rm(ns_rep)
        }

}
difference <- difftime(timeEnd, timeStart, units='mins')
message(paste(round(difference,digits = 1),"min  |  ",100*round(n/length(dat$s_t), digits = 2),"%  |  ",n, "of", length(dat$s_t)))    
message("Finisehd with set genes")

for (n in seq(length(listOfna_rep))) {
    if (n == 1) {na_rep <-               listOfna_rep[[n]]    }
    if (n != 1) {na_rep <- cbind(na_rep, listOfna_rep[[n]] )  }
}

for (n in seq(length(listOfns_rep))) {
    if (n == 1) {ns_rep <-               listOfns_rep[[n]]    }
    if (n != 1) {ns_rep <- cbind(ns_rep, listOfns_rep[[n]] )  }
}

mod_post <- post

mod_post$ns_rep <- ns_rep
mod_post$na_rep <- na_rep

saveRDS(mod_post, file=path2output)

