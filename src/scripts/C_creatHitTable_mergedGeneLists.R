#This script merges the tables with the genes mapped to the state transitions and calculates the frequency of specific transitions occurring either for a specified set of genes or against a background.



###############Parameters#########################
raw <- readRDS(snakemake@input[["i1"]] )
goCollect <- read.csv2(snakemake@input[["i2"]])
outputFile <- snakemake@output[["o"]]

ID <- snakemake@params[["cID"]]
intraCellIndex <- snakemake@params[["intraCellIndex"]]
tissueA <- snakemake@params[["tissueA"]]
tissueB <- snakemake@params[["tissueB"]]

if (FALSE) { #Debug section
  raw <- readRDS("/media/Enbio/Christoph/PhD-Work_DataUScrips/TransitionAnalysis_standAlone/transitionEnrichment/data/ZS/transitionMapped2Genes/transitionGeneDF_BmdMSC_59_TO_BmdMSC_58.rds" )  
    
  goCollect <- read.csv2("/media/Enbio/Christoph/PhD-Work_DataUScrips/TransitionAnalysis_standAlone/transitionEnrichment/data/ZS/mergedGeneDF.csv") 
  outputFile <- "media/Enbio/Christoph/PhD-Work_DataUScrips/TransitionAnalysis_standAlone/transitionEnrichment/data/ZS/transitionHitsCounted/transitionHitCount_BmdMSC_59_TO_BmdMSC_58.rds"
  ID <- "BmdMSC_59_TO_BmdMSC_58"
  intraCellIndex <- "1"
  tissueA <- "BmdMSC"
  tissueB <- "BmdMSC"
}


message(ID)
raw$transiton <- gsub("\\.", "-", raw$transiton) #if transitions are defined by a dot-notation it is replaced here

#################libraries############################
library(stringr)

#################Functions############################

regionName <- c("enhancer", "tss"  ,    "gene")[3]

getNumberOfStates <- function(raw){
  x <- str_split_fixed(raw$transiton, "-", 2)
  maxValue <- max(as.numeric(x))
  minValue <- min(as.numeric(x))
  return(c(minValue,maxValue))
}

creatInitHitDF <- function(numberList){
  compList <- c()
  diffState1List <- c()
  diffState2List <- c()
  for (diffState1 in seq(numberList[1]:numberList[2])) {
    for (diffState2 in seq(numberList[1]:numberList[2])) {
      compList <- c(compList, paste0(diffState1,"-",diffState2))
      diffState1List <- c(diffState1List, diffState1)
      diffState2List <- c(diffState2List, diffState2)
    }
  }
  completeDF <- data.frame(trans=compList,diffState1= diffState1List, diffState2= diffState2List)
}

countOccurence_GO <- function(initCompleteDF, indiGO = TRUE, indiBG = TRUE){ #indi go means that BG is the genes which are in neiger group, or indidualy for a.
  if (indiBG == FALSE) {
    geneBgDF <- raw[!(raw$external_gene_name %in% goCollect["external_gene_name"]) , ] # BG is all which is i neigther GO group
    tmpBG <- unique(geneBgDF[,c("transiton","external_gene_name")])
    transCountDfBG <- as.data.frame(table(tmpBG[ , c("transiton")]))
    colnames(transCountDfBG) <- c("trans", paste0("hits_BG"))
    transCountDfBG$tmp <- length(unique(geneBgDF$external_gene_name))
    names(transCountDfBG)[names(transCountDfBG) == 'tmp'] <-paste0('trails_BG')
    initCompleteDF <- merge(initCompleteDF, transCountDfBG, by = "trans", all.x = TRUE)
    initCompleteDF[,paste0('trails_BG')] <- length(unique(geneBgDF$external_gene_name))
    
  }

  for (geneList in unique(goCollect$runID)) {
    if (indiBG == TRUE) {
      geneBgDF <- raw[!(raw$external_gene_name %in% goCollect[(goCollect$runID == geneList ) ,"external_gene_name"]) , ] 
      tmpBG <- unique(geneBgDF[,c("transiton","external_gene_name")])
      transCountDfBG <- as.data.frame(table(tmpBG[ , c("transiton")]))
      colnames(transCountDfBG) <- c("trans", paste0("hits_BG_",geneList))
      transCountDfBG$tmp <- length(unique(geneBgDF$external_gene_name))
      names(transCountDfBG)[names(transCountDfBG) == 'tmp'] <-paste0('trails_BG_',geneList)
      initCompleteDF <- merge(initCompleteDF, transCountDfBG, by = "trans", all.x = TRUE)
      initCompleteDF[,paste0('trails_BG_',geneList)] <- length(unique(geneBgDF$external_gene_name))
    }
    if (indiGO == FALSE) {
      geneGoDF <- raw[ (raw$external_gene_name %in% goCollect[(goCollect$runID == geneList) ,"external_gene_name"]) , ]
      tmpGO <- unique(geneGoDF[,c("transiton","external_gene_name")])
      transCountDfGO <- as.data.frame(table(tmpGO[ , c("transiton")]))
      colnames(transCountDfGO) <- c("trans", paste0('hits_',geneList))
      transCountDfGO$tmp <- length(unique(geneGoDF$external_gene_name))
      names(transCountDfGO)[names(transCountDfGO) == 'tmp'] <-paste0('trails_',geneList)
      initCompleteDF <- merge(initCompleteDF, transCountDfGO, by = "trans", all.x = TRUE)
      initCompleteDF[,paste0('trails_',geneList)] <- length(unique(geneGoDF$external_gene_name))
    }
    if (indiGO == TRUE) {
    for (n in unique(goCollect[(goCollect$runID == geneList ) ,"goIndex"])) {
      geneGoDF <- raw[ (raw$external_gene_name %in% goCollect[(goCollect$goIndex == n & goCollect$runID == geneList) ,"external_gene_name"]) , ]
      tmpGO <- unique(geneGoDF[,c("transiton","external_gene_name")])
      transCountDfGO <- as.data.frame(table(tmpGO[ , c("transiton")]))
      colnames(transCountDfGO) <- c("trans", paste0('hits_',geneList,'_',n))
      transCountDfGO$tmp <- length(unique(geneGoDF$external_gene_name))
      names(transCountDfGO)[names(transCountDfGO) == 'tmp'] <-paste0('trails_',geneList,'_',n)
      initCompleteDF <- merge(initCompleteDF, transCountDfGO, by = "trans", all.x = TRUE)
      initCompleteDF[,paste0('trails_',geneList,'_',n)] <- length(unique(geneGoDF$external_gene_name))
    }
    }
}
  
  initCompleteDF[is.na(initCompleteDF)] <- 0
  return(initCompleteDF)
}

occurenceCountDF_FT <- countOccurence_GO(creatInitHitDF(getNumberOfStates(raw)), FALSE,TRUE )


#################Run it############################
counter <- 0
for (runID in unique(goCollect$runID)) {
  for (setORbg in c("BG_","")) {
    counter <- counter +1
    ifelse(setORbg == "BG_",setORbgPretty <- "BG",setORbgPretty <- "inSet" )
    tmp <- data.frame(occurenceCountDF_FT$trans, occurenceCountDF_FT[paste0("hits_",setORbg,runID)], occurenceCountDF_FT[paste0("trails_",setORbg,runID)], runID, setORbgPretty )
    colnames(tmp) <- c("trans","hits","trials","geneSet","SETorBG")
    if (counter == 1) {df <- tmp   }
    if (counter > 1) {df <- rbind(df,tmp) }
  }
}

df$combID <- ID
df$intraCell <- intraCellIndex
df$tissueA <- tissueA
df$tissueB <- tissueB


saveRDS(df, file = outputFile)

#occurenceCountDF <- countOccurence_Ran(creatInitHitDF(getNumberOfStates(raw)),1,1 )
#c("WU_Data/occurenceCount_MSC_PC_20200703.csv","WU_Data/occurenceCount_PC_HC_20200703.csv","WU_Data/occurenceCount_MSC_HC_20200703.csv")
#write.csv2(occurenceCountDF, file= file.path(parentPath,paste0("occurenceCountMerged",ID,".csv")), row.names = FALSE)
#save(completeDF, file = snakemake@output[["compDFName"]])

#################OLD############################
if (FALSE) {
countOccurence_Ran <- function(initCompleteDF,N=10, ratio = 0.8){
  uniqueList <- unique(goCollect$external_gene_name)
  for (n in 1:N) {
    geneBgDF <- raw[!(raw$external_gene_name %in% sample(uniqueList,length(uniqueList)*ratio)), ]
    tmpBG <- unique(geneBgDF[,c("transiton","external_gene_name")])
    transCountDfBG <- as.data.frame(table(tmpBG[ , c("transiton")]))
    colnames(transCountDfBG) <- c("trans", paste0("hits_BG_",n))
    transCountDfBG$tmp <- length(unique(geneBgDF$external_gene_name))
    names(transCountDfBG)[names(transCountDfBG) == 'tmp'] <-paste0('trails_BG_',n)
    initCompleteDF <- merge(initCompleteDF, transCountDfBG, by = "trans", all.x = TRUE)
    initCompleteDF[,paste0('trails_BG_',n)] <- length(unique(geneBgDF$external_gene_name))
    
    
    geneGoDF <- raw[ (raw$external_gene_name %in% sample(uniqueList,length(uniqueList)*ratio)), ]
    tmpGO <- unique(geneGoDF[,c("transiton","external_gene_name")])
    transCountDfGO <- as.data.frame(table(tmpGO[ , c("transiton")]))
    colnames(transCountDfGO) <- c("trans", paste0("hits_GO_",n))
    transCountDfGO$tmp <- length(unique(geneGoDF$external_gene_name))
    names(transCountDfGO)[names(transCountDfGO) == 'tmp'] <-paste0('trails_GO_',n)
    initCompleteDF <- merge(initCompleteDF, transCountDfGO, by = "trans", all.x = TRUE)
    initCompleteDF[,paste0('trails_GO_',n)] <- length(unique(geneGoDF$external_gene_name))
  }
  initCompleteDF[is.na(initCompleteDF)] <- 0
  return(initCompleteDF)
}
}