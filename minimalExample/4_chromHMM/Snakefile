import pandas as pd
import os.path

runID = config["runID"]
cellMarkFileLocation = str(config["cellMarkFileLocation"])+"/"
cfgLocation = str(config["cfgLocation"])+"/"

numberOfThreads = config["numberOfThreads"]
numberOfStates= str(config["numberOfStates"])

#global Params
chomHMM= config["chomHMM"]
chomSizeFile=config["chomSizeFile"]
chromHMMRef=config["chromHMMRef"]


#----------------------------------------



OutputFolder = os.path.dirname(os.path.abspath(cellMarkFileLocation))+"/output/"
#OutputFolder = cellMarkFileLocation

cfg_conso = pd.read_table(cfgLocation+"consolidated.cfg").set_index("ID", drop=False)
cfg_single= pd.read_table(cfgLocation+"unconsolidated.cfg").set_index("ID", drop=False)

cfg_cellmarkSingle= pd.read_table(cellMarkFileLocation+"/cellmarkfile_unconsolidated.cfg",names=["cell","mark","fileName"]) #must be created beforhand (run creat_cellMarkFile.R in test mode with the correct paths )

rule all:
    input:
        #expand(OutputFolder+"ZS/bed_single_noXY/{bedName}_noXY.bed", bedName=cfg_single.ID.unique()),
        #expand(OutputFolder+"ZS/bed_conso_noXY/{bedName}_noXY.bed", bedName=cfg_conso.ID.unique()),
        ####expand(OutputFolder+"preRun_cellmarkfiles/cellmarkfile_{cfgTyp}.cfg", cfgTyp=["consolidated","unconsolidated"]),
        #OutputFolder+"ChromHMM_out/binarized_Bed/consolidated/README",
        #OutputFolder+"ChromHMM_out/modle/"+numberOfStates+"_states/"+numberOfStates+"_states.stdout",
        #OutputFolder+"ChromHMM_out/binarized_Bed/single/README",
        #OutputFolder+"ChromHMM_out/modle/"+numberOfStates+"_states/modleSegmentation_Single/README",
        expand(OutputFolder+"ChromHMM_out/modle/"+numberOfStates+"_states/learnModle_Consolidated/{tissue}_"+numberOfStates+"_segments_CS.bed.idx", tissue=cfg_conso.tissue.unique()),
        expand(OutputFolder+"ChromHMM_out/modle/"+numberOfStates+"_states/modleSegmentation_Single/{cell}_"+numberOfStates+"_segments_CS.bed.idx",cell=cfg_cellmarkSingle.cell.unique()),
        OutputFolder+"/"+runID+"_runParams.txt"


rule runParams:
    params:
        runID
    output:
        OutputFolder+"/"+runID+"_runParams.txt"
    priority: 1
    shell:
        """
        echo {params} >> {output}
        date >> {output}
        cat Snakefile >> {output}
        echo "-----___----____---____---____-----" >> {output}
        #conda list >> {output} 
        """
       


rule removeXandY_single:
    input:
        lambda wc: cfg_single[cfg_single.ID == wc.bedName].folderPath.unique() +"/" + cfg_single[cfg_single.ID == wc.bedName].fileName.unique()
    output:
        OutputFolder+"ZS/bed_single_noXY/{bedName}_noXY.bed"
    shell:
        """cp {input} {output}"""

rule removeXandY_conso:
    input:
        lambda wc: cfg_conso[cfg_conso.ID == wc.bedName].folderPath.unique() +"/" + cfg_conso[cfg_conso.ID == wc.bedName].fileName.unique()
    output:
        OutputFolder+"ZS/bed_conso_noXY/{bedName}_noXY.bed"
    shell:
        """cp {input} {output}"""

#
#rule creatCellMarkFile:
#    input:
#        cfgLocation+"{cfgTyp}.cfg"
#        seedValue
#    output:
#        cfgLocation+"preRun_cellmarkfiles/cellmarkfile_{cfgTyp}.cfg"
#    script:
#        "scripts/creat_cellMarkFile.R"


rule Binarize_conso:
    input:
        bedFileName=[expand(OutputFolder+"ZS/bed_conso_noXY/{bedName}_noXY.bed",bedName=cfg_conso.ID.unique())],
        pCmFt=cellMarkFileLocation+"/cellmarkfile_consolidated.cfg"
    params:
        pLoc=chomHMM,
        pSize=chomSizeFile,
        ibed=OutputFolder+"ZS/bed_conso_noXY/",
        oPath=OutputFolder+"ChromHMM_out/binarized_Bed/consolidated"
    output:
        OutputFolder+"ChromHMM_out/binarized_Bed/consolidated/README"
    shell:
        """
        java -mx4000M -jar {params.pLoc} BinarizeBed -peaks -center -b 200 {params.pSize}  {params.ibed} {input.pCmFt} {params.oPath}
        echo 'Those are binirized bed files for the consolidated roadmap peaks' > {output}
        """

rule LearnModel:
    input:
        filler=OutputFolder+"ChromHMM_out/binarized_Bed/consolidated/README"
    params:
        pLoc=chomHMM,
        iPath=OutputFolder+"ChromHMM_out/binarized_Bed/consolidated",
        NoS=numberOfStates,
        cores=numberOfThreads,
        oPath=OutputFolder+"ChromHMM_out/modle/{numberOfStates}_states/learnModle_Consolidated",
        refrence=chromHMMRef
    output:
        o1=OutputFolder+"ChromHMM_out/modle/{numberOfStates}_states/learnModle_Consolidated/model_{numberOfStates}.txt",
        o2=OutputFolder+"ChromHMM_out/modle/{numberOfStates}_states/{numberOfStates}_states.stdout"
    threads: numberOfThreads
    shell:
        """
        java -mx4000M -jar {params.pLoc} LearnModel -p {params.cores} -nobrowser {params.iPath} {params.oPath} {params.NoS} {params.refrence} | tee {output.o2}
        """



rule Binarize_single:
    input:
        bedFileName=[expand(OutputFolder+"ZS/bed_single_noXY/{bedName}_noXY.bed",bedName=cfg_single.ID.unique())],
        pCmFt=cellMarkFileLocation+"/cellmarkfile_unconsolidated.cfg"
    params:
        pLoc=chomHMM,
        pSize=chomSizeFile,
        ibed=OutputFolder+"ZS/bed_single_noXY",
        oPath=OutputFolder+"ChromHMM_out/binarized_Bed/single"
    output:
        OutputFolder+"ChromHMM_out/binarized_Bed/single/README"
    shell:
        """
        java -mx4000M -jar {params.pLoc} BinarizeBed -peaks -center -b 200 {params.pSize}  {params.ibed} {input.pCmFt} {params.oPath}
        echo 'Those are binirized bed files for the Single roadmap peaks' > {output}
        """


rule segmentation_signle:
    input:
        i1=OutputFolder+"ChromHMM_out/modle/"+numberOfStates+"_states/learnModle_Consolidated/model_"+numberOfStates+".txt",
        itmp=OutputFolder+"ChromHMM_out/binarized_Bed/single/README"
    params:
        pLoc=chomHMM,
        binPath=OutputFolder+"ChromHMM_out/binarized_Bed/single/",
        oPath=OutputFolder+"ChromHMM_out/modle/"+numberOfStates+"_states/modleSegmentation_Single"
    output:
        o1=OutputFolder+"ChromHMM_out/modle/"+numberOfStates+"_states/modleSegmentation_Single/README"
        #o2=[expand(OutputFolder+"ChromHMM_out/modle/"+numberOfStates+"_states/modleSegmentation_Single/{cell}_"+numberOfStates+"_segments.bed",cell=list(set(cellMarkDF['cell'])))]
        #o2=[OutputFolder+"ChromHMM_out/modle/"+numberOfStates+"_states/modleSegmentation_Single/{cell}_"+numberOfStates+"_segments.bed"] #including this line will make snakemake run this command for ervery cell source
    shell:
        """
        java -mx4000M -jar {params.pLoc} MakeSegmentation -b 200 {input.i1} {params.binPath} {params.oPath}
        echo 'Those are segmentated bed files for the Single roadmap bin peaks' > {output.o1}
        """

rule makeChromHmmBedPretty_conso:
    input:
        filler=OutputFolder+"ChromHMM_out/modle/"+numberOfStates+"_states/learnModle_Consolidated/model_"+numberOfStates+".txt"
    params:
        numberOfS=numberOfStates,
        p2=OutputFolder+"ChromHMM_out/modle/"+numberOfStates+"_states/learnModle_Consolidated/{tissue}_"+numberOfStates+"_segments.bed",
    output:
        o1=temp(OutputFolder+"ChromHMM_out/modle/"+numberOfStates+"_states/learnModle_Consolidated/{tissue}_"+numberOfStates+"_segments_C.bed")
    script:
        "scripts/chromHMMVisBeds.R"


rule makeChromHmmBedIndexed_conso:
    input:
        OutputFolder+"ChromHMM_out/modle/"+numberOfStates+"_states/learnModle_Consolidated/{tissue}_"+numberOfStates+"_segments_C.bed"
    output:
        o2=OutputFolder+"ChromHMM_out/modle/"+numberOfStates+"_states/learnModle_Consolidated/{tissue}_"+numberOfStates+"_segments_CS.bed",
        o3=OutputFolder+"ChromHMM_out/modle/"+numberOfStates+"_states/learnModle_Consolidated/{tissue}_"+numberOfStates+"_segments_CS.bed.idx"
    shell:
        """
        igvtools sort {input} {output.o2}
        igvtools index {output.o2}
        """

rule makeChromHmmBedPretty_signle:
    input:
        i2=OutputFolder+"ChromHMM_out/modle/"+numberOfStates+"_states/modleSegmentation_Single/README"
    params:
        numberOfS=numberOfStates,
        i1=OutputFolder+"ChromHMM_out/modle/"+numberOfStates+"_states/modleSegmentation_Single/{cell}_"+numberOfStates+"_segments.bed"
    output:
        o1=temp(OutputFolder+"ChromHMM_out/modle/"+numberOfStates+"_states/modleSegmentation_Single/{cell}_"+numberOfStates+"_segments_C.bed")
    script:
        "scripts/chromHMMVisBeds.R"

rule makeChromHmmBedIndexed_signle:
    input:
        OutputFolder+"ChromHMM_out/modle/"+numberOfStates+"_states/modleSegmentation_Single/{cell}_"+numberOfStates+"_segments_C.bed"
    output:
        o2=OutputFolder+"ChromHMM_out/modle/"+numberOfStates+"_states/modleSegmentation_Single/{cell}_"+numberOfStates+"_segments_CS.bed",
        o3=OutputFolder+"ChromHMM_out/modle/"+numberOfStates+"_states/modleSegmentation_Single/{cell}_"+numberOfStates+"_segments_CS.bed.idx",
    shell:
        """
        igvtools sort {input} {output.o2}
        igvtools index {output.o2}
        """

