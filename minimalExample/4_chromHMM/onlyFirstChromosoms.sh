#!/bin/bash

# Folder path containing the .bed files
folder_path="/media/Enbio/Christoph/PhD-Work_DataUScrips/BATH/minimalExample/4_chromHMM/out/output/ChromHMM_out/modle/5_states/modleSegmentation_Single/ignore"

# Iterate through each .bed file in the folder
for file_path in "$folder_path"/*segments_CS.bed; do
    # Perform the awk operation and overwrite the file
    awk -F'\t' '$1=="chr1"' "$file_path" > "$file_path.tmp" 
done
