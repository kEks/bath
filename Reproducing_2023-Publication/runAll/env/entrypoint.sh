#!/bin/bash --login
set -e


cd /home/keks/app/runAll

echo "Starting the conda fullRun_env on startup (entrypoint) does not work. Thus run:"
echo "conda activate /home/keks/miniconda3/envs/fullRun_env"
echo "snakemake --cores 6 --notemp --use-conda --conda-prefix /home/keks/app/CondaSnakemake"

exec "$@"
