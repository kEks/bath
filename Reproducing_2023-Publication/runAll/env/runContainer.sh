#podman build -t runall_dez23img -f env/Dockerfile .

podman run -it --rm \
--volume /media/Enbio/Christoph/PhD-Work_DataUScrips/BATH/Reproducing_2023-Publication/MSC_PC_HC:/home/keks/app/MSC_PC_HC \
--volume /media/Enbio/Christoph/PhD-Work_DataUScrips/BATH/Reproducing_2023-Publication/ROADMAP:/home/keks/app/ROADMAP \
--volume /media/Enbio/Christoph/PhD-Work_DataUScrips/BATH/Reproducing_2023-Publication/runAll:/home/keks/app/runAll \
runall_dec23img bash #interactiv

#runall_1804img bash #interactiv
