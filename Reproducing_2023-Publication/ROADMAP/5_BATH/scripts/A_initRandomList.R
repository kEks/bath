set.seed(1)

options(warn = 2)
library(biomaRt)
args <- commandArgs(trailingOnly = TRUE)
#refSet <- "hsapiens_gene_ensembl"
refSet <- args[1] #hsapiens_gene_ensembl

numberOfargs <- length(args)

for (n in seq(numberOfargs)) {
  print(n)
  print(args[n])
  print("------")
  write.table(data.frame("randomfiller"), file = args[n])
}