library(ggplot2)
library(ggplotify)


args <- commandArgs(trailingOnly = TRUE)

setOfInteress <- args[1]
pathToCLESdf <- args[2]
cfgStatePath  <- args[3]

pathPlotOut <- args[4]


if (FALSE) {
    setOfInteress <- "chondro"
  pathToCLESdf <- "/media/Enbio/Christoph/PhD-Work_DataUScrips/TransitionAnalysis_standAlone/transEnriTool/transEnriTool_out/ROADMAP/ROADMAP_2022-05-30_seed1/ZS/CLESTables/CLESTable.rds"
  cfgStatePath  <- "/media/Enbio/Christoph/PhD-Work_DataUScrips/TransitionAnalysis_standAlone/transEnriTool/transEnriTool_out/ROADMAP/ROADMAP_2022-05-30_seed1/Input/stateOrder.cfg"
  pathPlotOut <- paste0("/media/Enbio/Christoph/tmp/CLES_",setOfInteress,".png")
}

resDF <- readRDS(pathToCLESdf)
cfgDf <- read.table(cfgStatePath,sep="\t",skip = 1,header = TRUE,comment.char = "")

ebenen_for_p <- c(0.0001,0.001, 0.01, 0.1 )
ebenen_for_p <- c(ebenen_for_p,0.5,1-rev(ebenen_for_p))

stateColorList <- cfgDf[order(cfgDf$Order),"Color"]
stateColorList <- c(as.character( stateColorList)) 


##### Plot  
resDF_subSet <- resDF[resDF$geneSet_g == setOfInteress, ]
resDF_subSet <- resDF_subSet[,c("fromState" ,"toState" ,"fromTissue", "toTissue" ,"CLESLevel_of_p")]


myGridPlot <- function(resDF_subSet_inner ){
  p<-ggplot(resDF_subSet_inner,aes(toTissue,fromTissue,fill= CLESLevel_of_p))+
    geom_tile()+
    scale_fill_brewer(name="CLES of P",palette = "Spectral",limits=rev(paste0(c(rep("<", (length(ebenen_for_p)-1)/2 ),"∼", rep(">", (length(ebenen_for_p)-1)/2 )),ebenen_for_p)))+
    #geom_text(aes(label = round(log(cles/(1-cles)), 1)))+
    #scale_fill_gradientn(name="CLES",palette = "Set1")+
    #scale_fill_gradientn(colours = c("red", "grey92", "grey92", "grey92", "grey92", "grey92", "green"),name="CLES",limits=c(0,1))+
    facet_grid(fromState ~toState,switch="both")+
    theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1),panel.background = element_blank())+
    ggtitle(setOfInteress) + ylab("From") + xlab("To")
  
  
  g <- ggplot_gtable(ggplot_build(p))
  strip_both <- which(grepl('strip-', g$layout$name))
  fills <- rep(stateColorList ,2)
  k <- 1
  for (i in strip_both) {
    j <- which(grepl('rect', g$grobs[[i]]$grobs[[1]]$childrenOrder))
    g$grobs[[i]]$grobs[[1]]$children[[j]]$gp$fill <- fills[k]
    k <- k+1
  }
  return(list(p, g))
}




print(paste("Creating Plots for CLES of P:",setOfInteress) )
plotList <- myGridPlot(resDF_subSet)
p <- as.ggplot(plotList[[2]])

ggsave(plot=p ,filename = pathPlotOut,width = 13, height = 13)




