data {
  int<lower=1> Ns; // Number set
  int<lower=1> Na; // Number all
  
  int<lower=1> Nt; //
  int<lower=1> Nb; //
  int<lower=1> Ng; //
    
  int s_total[Ns]; 
  int s_hits[Ns]; 
  int<lower=1> s_t[Ns];
  int<lower=1> s_b[Ns];
  int<lower=1> s_g[Ns];
  
  int a_total[Na]; 
  int a_hits[Na]; 
  int<lower=1> a_t[Na];
  int<lower=1> a_b[Na];
}
parameters {
  real<lower=0> Ps_alpha[Nt,Nb,Ng];
  real<lower=0> Ps_beta[Nt,Nb,Ng];
  
  real<lower=0> Pa_alpha[Nt,Nb];
  real<lower=0> Pa_beta[Nt,Nb];
  
  
  real<lower=0> P_alpha_mult_m;
  real<lower=0> P_beta_mult_m;
}

model {
  for(n in 1:Ns){
    s_hits[n] ~ beta_binomial(s_total[n], Ps_alpha[s_t[n], s_b[n], s_g[n]], Ps_beta[s_t[n], s_b[n], s_g[n]]); 
  }
  for(n in 1:Na){
    a_hits[n] ~ beta_binomial(a_total[n], Pa_alpha[a_t[n], a_b[n]], Pa_beta[a_t[n], a_b[n]]); 
  }
  
  for(i in 1:Nt){
    for(j in 1:Nb){
      Ps_alpha[i,j] ~ exponential(P_alpha_mult_m);
      Ps_beta[i,j] ~ exponential(P_beta_mult_m);
    }
    Pa_alpha[i] ~ exponential(P_alpha_mult_m);
    Pa_beta[i] ~ exponential(P_beta_mult_m);
  }
  
  P_alpha_mult_m ~ exponential(10);
  P_beta_mult_m ~ exponential(10);
}
generated quantities { // for the PPC and loo
  real ns_rep[Ns];
  real na_rep[Na];
  
  for(n in 1:Ns){
    ns_rep[n] = beta_binomial_rng(s_total[n], Ps_alpha[s_t[n], s_b[n], s_g[n]], Ps_beta[s_t[n], s_b[n], s_g[n]]);
  }
  
  for(n in 1:Na){
    na_rep[n] = beta_binomial_rng(a_total[n], Pa_alpha[a_t[n], a_b[n]], Pa_beta[a_t[n], a_b[n]]);
  }
}

