#script to map cromHMM states to genes.

#in chromHMM they are called [CELLTYP]_[STATE-NR]_segments.bed
library(readr)
#library(dplyr)
library(data.table)
options(scipen = 999)


readStateBed <-function(stateBedPath, relTime) {

  
  state_df <- read_delim(stateBedPath, 
                         "\t", escape_double = FALSE, col_names = c("chr","start","end","state"), 
                         trim_ws = TRUE)
  state_df <- state_df[(state_df$start != 0 ), ] # remove beginning of chromHMM since its urealaible
  state_df$ID <- paste0(state_df$chr,"-",state_df$start,"-",state_df$end)
  colnames(state_df) <- paste0(colnames(state_df),".",relTime)
  state_bed <- state_df[,1:3]
  
  return(list(state_df,state_bed))
}

mergeStateFiles <- function(stateListA, stateListB){
  df_A <- as.data.table(stateListA[[2]])
  df_B <- as.data.table(stateListB[[2]])
  
  setkeyv(df_A, names(df_A))
  setkeyv(df_B, names(df_B))
  ans <- foverlaps(df_B, df_A, nomatch=0L)

  df <- as.data.frame((ans))
  
  df[c('start.overlap', 'end.overlap')] <- t(apply(df, 1, function(x) 
    sort(x)[c(length(x)-3, length(x) - 2)]))
  
  # minoverlap
  df$start.overlap <- as.integer(as.character(df$start.overlap))
  df$end.overlap <- as.integer(as.character(df$end.overlap))
  df <- df[abs(df$start.overlap - df$end.overlap)> 10 , ]
  
  df$ID.A <- paste0(df$chr.B,"-",df$start.A,"-",df$end.A)
  df$ID.B <- paste0(df$chr.B,"-",df$start.B,"-",df$end.B)
  
  df <- merge(df, stateListA[[1]][,c("state.A","ID.A")], by ="ID.A")
  df <- merge(df, stateListB[[1]][,c("state.B","ID.B")], by ="ID.B")
  df$transiton <- paste(substring(df$state.A,2),substring(df$state.B,2) ,sep = ".")

  transiton_df <-  df[,c("chr.B","start.overlap","end.overlap","transiton")]
  names(transiton_df)[names(transiton_df) == 'chr.B'] <- 'chr.overlap'  
  transiton_df$ID.Overlap <- paste0(transiton_df$chr.overlap,"-",transiton_df$start.overlap,"-",transiton_df$end.overlap)
  transiton_bed <- transiton_df[, c("chr.overlap","start.overlap","end.overlap")]
  
  return(list(transiton_df, transiton_bed))
}  

creatVisTransbed <- function(transiton_df, outPutPath = getwd(),outputID){

  if("external_gene_name"%in% colnames(transiton_df)){
         tmp <- transiton_df[,c("chr.overlap","start.overlap","end.overlap","transiton","external_gene_name")]
         outPutPath <- file.path(outPutPath,paste0("transitionGeneVis_",outputID,".bed"))
         }
  if(!("external_gene_name"%in% colnames(transiton_df))){
       tmp <- transiton_df[,c("chr.overlap","start.overlap","end.overlap","transiton")]
       outPutPath <- file.path(outPutPath,paste0("transitonVis_",outputID,".bed"))}

  rgbValue <- c()
  rbgTable <- as.data.frame(col2rgb(sample(grDevices::colors()[grep('gr(a|e)y', grDevices::colors(), invert = T)], length(unique(tmp$transiton ) ))))
  for (variable in colnames(rbgTable)) {
    rgbValue <-c(rgbValue, paste(rbgTable[,variable][1],rbgTable[,variable][2],rbgTable[,variable][3], sep = ","))
  }
  tmpA <- data.frame(transiton = unique(tmp$transiton), color = rgbValue)
  

  
  tmp$ka <- 1000
  tmp$strand <- "."
  tmp$startAgain <- tmp$start.overlap
  tmp$endAgian <- tmp$end.overlap
  tmpB <- colnames(tmp)
  tmp <- merge(tmp, tmpA, by="transiton")
  tmp <- tmp[,c(tmpB, "color")]
  if("external_gene_name"%in% colnames(transiton_df)){
    tmp$transiton <- paste0(tmp$transiton,"_",tmp$external_gene_name)
    tmp$external_gene_name <- NULL
  }
  print(outPutPath)
  write.table(tmp,file =outPutPath, sep = "\t",row.names = FALSE, col.names = FALSE, quote = FALSE)
 
}

MergeTransitionWithGeneAnno <- function(annot_bed,mergedStateList,outputFolder,outputID){
  df_A <- as.data.table(annot_bed)
  df_B <- as.data.table(mergedStateList[[2]])
  
  setkeyv(df_A, names(df_A))
  setkeyv(df_B, names(df_B))
  ans <- foverlaps(df_B, df_A, nomatch=0L)
  
  ans$ID.annot <- paste0(ans$chr.overlap,"-",ans$start.annot,"-",ans$end.annot)
  ans$ID.Overlap <- paste0(ans$chr.overlap,"-",ans$start.overlap,"-",ans$end.overlap)
  ans <- ans[,c("ID.annot","ID.Overlap")]
  ans <- merge(annot_df, ans, by="ID.annot")
  ans <- merge(ans, mergedStateList[[1]], by="ID.Overlap")
  creatVisTransbed(ans,outputFolder,outputID)
  return(ans)
}

creatCombTable <- function(dataRelation){
  
  
  everyComb <- combn(dataRelation$replicate, 2)
  
  everyComb <- as.data.frame(t(everyComb))
  
  diffOrder <- c("H1","H1dMSC","BmdMSC","CfBmdMSC")
  
  counter <- 0
  for (combRow in rownames(everyComb)) {
    counter <- counter + 1 
    A <- as.character(everyComb[combRow,1])
    B <- as.character(everyComb[combRow,2])
    print(paste(A,B))
    A1 <- strsplit(A,"_")[[1]][1]
    B1 <- strsplit(B,"_")[[1]][1]
    if (match(A1, diffOrder) > match(A1, diffOrder)) { newRow <- list(repA = A, repB = B, tissueA =A1,tissueB=B1) }
    if (match(B1, diffOrder) <=  match(B1, diffOrder)) { newRow <- list(repA = B, repB = A, tissueA =B1,tissueB=A1) }
    
    if (counter ==1) { df <- as.data.frame(newRow) }
    if (counter >1 ) { df <- rbind(df, as.data.frame(newRow)) }
  }
  return(df)
}
##############Paramerters######################
args <- commandArgs(trailingOnly = TRUE)
refDataset <- args[1] #"/media/Enbio/Christoph/PhD-Work_DataUScrips/TransitionAnalysis_standAlone/transitionEnrichment/data/ZS/geneRefFiles/annot_df_refrenceSet.Rds"

#dataRelation <- read_delim("/media/Enbio/Christoph/PhD-Work_DataUScrips/TransitionAnalysis_standAlone/transitionEnrichment/data/input/dataRelation.cfg","\t", trim_ws = TRUE)
outputFolder <- dirname(args[5]) #dirname(snakemake@output[[1]][1])#"/media/Enbio/Christoph/PhD-Work_DataUScrips/TransitionAnalysis_standAlone/transitionEnrichment/data/ZS/transitionMapped2Genes/"
outPutFileRds <- args[5]
ID <- args[4] #snakemake@params[[1]][1]
fileA <-  args[2] #snakemake@input[[1]]["fA"]
fileB <-  args[3] #snakemake@input[[1]]["fB"]

if (FALSE) {
  refDataset <- "/media/Enbio/Christoph/PhD-Work_DataUScrips/TransitionAnalysis_standAlone/transitionEnrichment/MSC_PC_HC/MSC_testEnv/ZS/geneRefFiles/annot_df_refrenceSet.Rds"
  outputFolder <- dirname("/media/Enbio/Christoph/PhD-Work_DataUScrips/TransitionAnalysis_standAlone/transitionEnrichment/MSC_PC_HC/MSC_testEnv/ZS/transitionMapped2Genes/transitionGeneDF_HC_ranB_TO_HC_ranA_test.rds") 
  outPutFileRds <- "/media/Enbio/Christoph/PhD-Work_DataUScrips/TransitionAnalysis_standAlone/transitionEnrichment/MSC_PC_HC/MSC_testEnv/ZS/transitionMapped2Genes/transitionGeneDF_HC_ranB_TO_HC_ranA_test.rds"
  ID <- "test"
  fileA <-  "/media/Enbio/Christoph/PhD-Work_DataUScrips/TransitionAnalysis_standAlone/ChromHMM/chromHMM_WUPCHC_2021-09-27/data/peakOut/chromHMM/modle/15_states/modleSegmentation_Single/HC_ranB_15_segments.bed"
  fileB <-  "/media/Enbio/Christoph/PhD-Work_DataUScrips/TransitionAnalysis_standAlone/ChromHMM/chromHMM_WUPCHC_2021-09-27/data/peakOut/chromHMM/modle/15_states/modleSegmentation_Single/HC_ranA_15_segments.bed"
  	
}

print(refDataset)
print(outputFolder)
print(outPutFileRds)
print(ID)
print(fileA)
print(fileB)


############ Run it#########################
#Gene Infos


#ensembl <- useMart("ensembl", dataset="mmusculus_gene_ensembl")
#annot_df<-getBM(c("ensembl_gene_id", "external_gene_name", "chromosome_name", "start_position", "end_position","gene_biotype"), mart=ensembl)
#save(annot_df, file = "/media/ubuntuDisk/home/knalli/Dropbox/Phd/PhD-Work_Drop/h_vs_p_rnaseq_chromhmm_2020/StateTransitions/transitionEnrichment/ZS/annot_df_20200512.Rda")
annot_df <- readRDS(refDataset)

annot_df <- annot_df[annot_df$gene_biotype == "protein_coding" , c("ensembl_gene_id", "external_gene_name", "chromosome_name", "start_position", "end_position")]
annot_df$chr.annot <- paste0("chr",annot_df$chromosome_name)
annot_df$chromosome_name <- NULL
names(annot_df)[names(annot_df) == 'start_position'] <- 'start.annot'
names(annot_df)[names(annot_df) == 'end_position'] <- 'end.annot'

annot_df$ID.annot <- paste0(annot_df$chr.annot,"-",annot_df$start.annot,"-",annot_df$end.annot)

annot_bed <- annot_df[,c("chr.annot","start.annot","end.annot")]

###########

#combTable <- creatCombTable(dataRelation)
stateListA <- readStateBed(fileA,"A")
stateListB <- readStateBed(fileB,"B")
print("after Readin")
mergedStateList <- mergeStateFiles(stateListA,stateListB)
print("after merge")
creatVisTransbed(mergedStateList[[1]],outputFolder,ID)
print("after creatvis")
transitionGeneDF <- MergeTransitionWithGeneAnno(annot_bed,mergedStateList,outputFolder,ID)
print("after merge")

saveRDS(transitionGeneDF, file = outPutFileRds)

print(head(transitionGeneDF))
###########OLD##############
if (FALSE) {
  

  #chrommHMMOutPath <- "/media/Enbio/Christoph/PhD-Work_DataUScrips/FremdDaten/Roadmap/download_Bed/data/procData/chromHMM/modle/15_states/modleSegmentation_Single"
  
  combTable <- creatCombTable(dataRelation)
  
  for (n in rownames(combTable)) {
    ID <-    paste0(as.character(combTable[n,"repA"]),"_vs_",as.character(combTable[n,"repB"]))
    fileA <- paste0(dataRelation[dataRelation$replicate== as.character(combTable[n,"repA"]),"path"],"/",dataRelation[dataRelation$replicate== as.character(combTable[n,"repA"]),"fileName"])
    fileB <- paste0(dataRelation[dataRelation$replicate== as.character(combTable[n,"repB"]),"path"],"/",dataRelation[dataRelation$replicate== as.character(combTable[n,"repB"]),"fileName"])
  
    stateListA <- readStateBed(fileA,"A")
    stateListB <- readStateBed(fileB,"B")
    
    mergedStateList <- mergeStateFiles(stateListA,stateListB)
    #creatVisTransbed(mergedStateList[[1]],outputFolder,ID)
    transitionGeneDF <- MergeTransitionWithGeneAnno(annot_bed,mergedStateList,outputFolder,ID)
    
    
    saveRDS(transitionGeneDF, file = paste0(outputFolder,"transitionGeneDF_",ID,".rds"))
    }
}

