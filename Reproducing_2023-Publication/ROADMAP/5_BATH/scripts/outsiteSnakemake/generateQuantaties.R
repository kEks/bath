
library(readr)
library(extraDistr)
library(svMisc)


modle <- read_rds("/media/Enbio/Christoph/PhD-Work_DataUScrips/BATH/Reproducing_2023-Publication/ROADMAP/5_BATH/ZS/modle/bayesModle.Rds")

post <- rstan::extract(modle)
rm(modle)

dat <- read_rds("/media/Enbio/Christoph/PhD-Work_DataUScrips/BATH/Reproducing_2023-Publication/ROADMAP/5_BATH/ZS/modle/modleInput_dat.rds")

set.seed(1)
randomDraws <- sample(seq(8000), 2000)

# creating rep
timeStart<-Sys.time()
listOfna_rep <- list()
for (n in seq(length(dat$a_t))) {
    if (n %% 1000 == 0) { 
        timeEnd<-Sys.time()
        difference <- difftime(timeEnd, timeStart, units='mins')
        print(paste(round(difference,digits = 1),"min  |  ",round(n/length(dat$a_t), digits = 2),"%  |  ",n, "of", length(dat$a_t)))    }
    Pa_alpha_tmp <- post$Pa_alpha[randomDraws, dat$a_t[n], dat$a_b[n]]
    Pa_beta_tmp <- post$Pa_beta[randomDraws, dat$a_t[n], dat$a_b[n]]
    N <- dat$a_total[n]
    na_rep_tmp <- mapply(rbbinom, n=1, size=N, alpha=Pa_alpha_tmp , beta=Pa_beta_tmp )
    if (n == 1 | (n-1) %% 10000 == 0) {    na_rep <- na_rep_tmp    }
    if (n != 1 & (n-1) %% 10000 != 0) {  na_rep <- cbind(na_rep, na_rep_tmp)}
    
    if (n %% 10000 == 0 | n == length(dat$a_t)) { 
        listOfna_rep[[paste0("upTo",n,"Rows")]]<- na_rep
        rm(na_rep)
        }
}
difference <- difftime(timeEnd, timeStart, units='mins')
print(paste(round(difference,digits = 1),"min  |  ",round(n/length(dat$a_t), digits = 2),"  |  ",n, "of", length(dat$a_t)))    
print("Finished with all genes")


timeStart<-Sys.time()
listOfns_rep <- list()
for (n in seq(length(dat$s_t))) {
    if (n %% 1000 == 0) { 
        timeEnd<-Sys.time()
        difference <- difftime(timeEnd, timeStart, units='mins')
        print(paste(round(difference,digits = 1),"min  |  ",round(n/length(dat$s_t), digits = 2),"%  |  ",n, "of", length(dat$s_t)))    }
    Ps_alpha_tmp <- post$Ps_alpha[randomDraws, dat$s_t[n], dat$s_b[n], dat$s_g[n]]
    Ps_beta_tmp <- post$Ps_beta[randomDraws, dat$s_t[n], dat$s_b[n], dat$s_g[n]]
    N <- dat$s_total[n]
    ns_rep_tmp <- mapply(rbbinom, n=1, size=N, alpha=Ps_alpha_tmp , beta=Ps_beta_tmp )
    if (n == 1 | (n-1) %% 10000 == 0) {    ns_rep <- ns_rep_tmp    }
    if (n != 1 & (n-1) %% 10000 != 0) {  ns_rep <- cbind(ns_rep, ns_rep_tmp)}
    
    if (n %% 10000 == 0 | n == length(dat$s_t)) { 
        listOfns_rep[[paste0("upTo",n,"Rows")]]<- ns_rep
        rm(ns_rep)
        }

}
difference <- difftime(timeEnd, timeStart, units='mins')
print(paste(round(difference,digits = 1),"min  |  ",round(n/length(dat$s_t), digits = 2),"%  |  ",n, "of", length(dat$s_t)))    
print("Finisehd with set genes")

for (n in seq(length(listOfna_rep))) {
    print(n)
    if (n == 1) {na_rep <-               listOfna_rep[[n]]    }
    if (n != 1) {na_rep <- cbind(na_rep, listOfna_rep[[n]] )  }
}

for (n in seq(length(listOfns_rep))) {
    print(n)
    if (n == 1) {ns_rep <-               listOfns_rep[[n]]    }
    if (n != 1) {ns_rep <- cbind(ns_rep, listOfns_rep[[n]] )  }
}

mod_post <- post

mod_post$ns_rep <- ns_rep
mod_post$na_rep <- na_rep

saveRDS(mod_post, file="/media/Enbio/Christoph/PhD-Work_DataUScrips/BATH/Reproducing_2023-Publication/ROADMAP/5_BATH/ZS/modle/bayesModle_postAdded.Rds")


