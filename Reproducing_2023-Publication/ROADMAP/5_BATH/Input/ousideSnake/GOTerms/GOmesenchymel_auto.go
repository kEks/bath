GO:0044338 #name: canonical Wnt signaling pathway involved in mesenchymal stem cell differentiation
GO:0072497 #name: mesenchymal stem cell differentiation
GO:0097168 #name: mesenchymal stem cell proliferation
GO:1902460 #name: regulation of mesenchymal stem cell proliferation
GO:1902461 #name: negative regulation of mesenchymal stem cell proliferation
GO:1902462 #name: positive regulation of mesenchymal stem cell proliferation
GO:1905319 #name: mesenchymal stem cell migration
GO:1905320 #name: regulation of mesenchymal stem cell migration
GO:1905321 #name: negative regulation of mesenchymal stem cell migration
GO:1905322 #name: positive regulation of mesenchymal stem cell migration
GO:2000739 #name: regulation of mesenchymal stem cell differentiation
GO:2000740 #name: negative regulation of mesenchymal stem cell differentiation
GO:2000741 #name: positive regulation of mesenchymal stem cell differentiation
