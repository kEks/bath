GO:0005020 #name: stem cell factor receptor activity
GO:0005173 #name: stem cell factor receptor binding
GO:0017145 #name: stem cell division
GO:0019827 #name: stem cell population maintenance
GO:0030718 #name: germ-line stem cell population maintenance
GO:0036098 #name: male germ-line stem cell population maintenance
GO:0036099 #name: female germ-line stem cell population maintenance
GO:0042078 #name: germ-line stem cell division
GO:0072089 #name: stem cell proliferation
GO:0072091 #name: regulation of stem cell proliferation
GO:0098728 #name: germline stem cell asymmetric division
GO:0098729 #name: germline stem cell symmetric division
GO:1902455 #name: negative regulation of stem cell population maintenance
GO:1902459 #name: positive regulation of stem cell population maintenance
GO:1902460 #name: regulation of mesenchymal stem cell proliferation
GO:1902461 #name: negative regulation of mesenchymal stem cell proliferation
GO:1902462 #name: positive regulation of mesenchymal stem cell proliferation
GO:1904671 #name: negative regulation of cell differentiation involved in stem cell population maintenance
GO:1904838 #name: regulation of male germ-line stem cell asymmetric division
GO:1904839 #name: negative regulation of male germ-line stem cell asymmetric division
GO:1904840 #name: positive regulation of male germ-line stem cell asymmetric division
GO:1905452 #name: obsolete canonical Wnt signaling pathway involved in regulation of stem cell proliferation
GO:1905474 #name: canonical Wnt signaling pathway involved in stem cell proliferation
GO:2000035 #name: regulation of stem cell division
GO:2000036 #name: regulation of stem cell population maintenance
GO:2000647 #name: negative regulation of stem cell proliferation
GO:2000648 #name: positive regulation of stem cell proliferation
GO:2000736 #name: regulation of stem cell differentiation
GO:2000737 #name: negative regulation of stem cell differentiation
GO:2000738 #name: positive regulation of stem cell differentiation
GO:2000739 #name: regulation of mesenchymal stem cell differentiation
GO:2000740 #name: negative regulation of mesenchymal stem cell differentiation
GO:2000741 #name: positive regulation of mesenchymal stem cell differentiation
