GO:0002062 #name: chondrocyte differentiation
GO:0002063 #name: chondrocyte development
GO:0003413 #name: chondrocyte differentiation involved in endochondral bone morphogenesis
GO:0003414 #name: chondrocyte morphogenesis involved in endochondral bone morphogenesis
GO:0003415 #name: chondrocyte hypertrophy
GO:0003433 #name: chondrocyte development involved in endochondral bone morphogenesis
GO:0032330 #name: regulation of chondrocyte differentiation
GO:0032331 #name: negative regulation of chondrocyte differentiation
GO:0032332 #name: positive regulation of chondrocyte differentiation
GO:0035988 #name: chondrocyte proliferation
GO:0044566 #name: chondrocyte activation
GO:0061181 #name: regulation of chondrocyte development
GO:0061182 #name: negative regulation of chondrocyte development
GO:0090171 #name: chondrocyte morphogenesis
GO:1902731 #name: negative regulation of chondrocyte proliferation
GO:1902732 #name: positive regulation of chondrocyte proliferation
GO:1902738 #name: regulation of chondrocyte differentiation involved in endochondral bone morphogenesis
GO:1902761 #name: positive regulation of chondrocyte development
GO:1903041 #name: regulation of chondrocyte hypertrophy
GO:1903042 #name: negative regulation of chondrocyte hypertrophy
GO:1903043 #name: positive regulation of chondrocyte hypertrophy
